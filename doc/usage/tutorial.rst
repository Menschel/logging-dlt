Tutorial
========

Setting up a Stream Adapter
---------------------------

The approach is fairly simple. Create a Serial Port object and pass it to the Constructor of SerialDltStreamLogAdapter.
Assuming you have a port /dev/ttyDLT and your DUT has a baudrate of 500000, you would do the following.

.. code-block:: python

    from serial import Serial
    from dlt.adapter import SerialDltStreamLogAdapter
    import time
    from logging import getLogger, StreamHandler, DEBUG

    ser = Serial(port="/dev/ttyDLT", baudrate=500000)
    dlt_adapter = SerialDltStreamLogAdapter(ser=ser)
    logger = getLogger("dlt")
    handler = StreamHandler()
    logger.addHandler(handler)
    logger.setLevel(DEBUG)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass


If you have something that produces dlt on the other side of the serial port, you will now see output in console.


Using logging-dlt as a pytest fixture
-------------------------------------

Pytest is a python test framework that does an exceptionally well job for doing functional tests on embedded systems.
The main benefits in our case are pytest fixtures and automatic catching of log records that pass through pythons logging
module.
When you write something like this into your conftest.py, you end up with dlt logs inside your regular test logs
which is very convenient.

.. code-block:: python

    import pytest
    from serial import Serial
    from dlt.adapter import SerialDltStreamLogAdapter

    @pytest.fixture(scope="session", autouse=True)
    def dlt_logger:
        ser = Serial(port="/dev/ttyDLT", baudrate=500000)
        dlt_adapter = SerialDltStreamLogAdapter(ser=ser)
        yield dlt_adapter
