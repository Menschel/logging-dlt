""" module:: test.mocks
    :synopsis: A Mock Serial Port to be used in tests as a source for logging_dlt.
    moduleauthor:: Patrick Menschel (menschel.p@posteo.de)
    license:: CC-BY-NC
"""
import time
from pathlib import Path
from queue import Queue
from random import randint
from socket import socket
from typing import Optional

from serial import Serial

import threading
import socket

from logging_dlt.protocol import generate_dlt_log_record, DLT_MAGIC_SERIAL, DltPayloadType, generate_payload, DltHeaderBit, \
    generate_extended_header, generate_standard_header_extra, generate_standard_header, DltLogLevel


def gen_dlt_msg() -> bytearray:
    ret = bytearray()
    ret.extend(DLT_MAGIC_SERIAL)
    ret.extend(generate_dlt_log_record(payload_text="some_test",
                                       ecuid="ECU1",
                                       sessionid="SESS",
                                       timestamp=0.1,
                                       applicationid="APP1",
                                       contextid="CONT",
                                       endianess="little"))
    return ret


def gen_invalid_dlt_msg() -> bytearray:
    data = bytearray()
    data.extend(DLT_MAGIC_SERIAL)
    payload_text = "some text in invalid frame"
    endianess = "little"
    ecuid = "ECU1"
    sessionid = "SESS"
    timestamp = 0.1
    applicationid = "APP1"
    contextid = "CONT"
    log_level = DltLogLevel.INFO
    verbose = False

    payload_type = DltPayloadType.STRING
    record_type, msb_first, payload = generate_payload(payload_type=payload_type,
                                                       payload_text=payload_text,
                                                       endianess=endianess)
    record_type = 0xE  # invalid record type
    flags = DltHeaderBit(0)
    if msb_first:
        flags |= DltHeaderBit.MSB_FIRST

    number_of_arguments = 1
    extended_header = generate_extended_header(record_type=record_type,
                                               applicationid=applicationid,
                                               contextid=contextid,
                                               log_level=log_level,
                                               verbose=verbose,
                                               number_of_arguments=number_of_arguments,
                                               )
    flags |= DltHeaderBit.USE_EXTENDED_HEADER

    extra_flags, standard_header_extra = generate_standard_header_extra(ecuid=ecuid,
                                                                        sessionid=sessionid,
                                                                        timestamp=timestamp)

    flags |= extra_flags
    standard_header = generate_standard_header(flags=flags,
                                               record_count=1,
                                               record_length=len(payload) + len(extended_header) + len(
                                                   standard_header_extra) + 4
                                               )
    data.extend(standard_header)
    data.extend(standard_header_extra)
    data.extend(extended_header)
    data.extend(payload)
    return data


def gen_invalid_payload_dlt_msg() -> bytearray:
    data = bytearray()
    data.extend(DLT_MAGIC_SERIAL)
    payload_text = "some text in invalid frame"
    endianess = "little"
    ecuid = "ECU1"
    sessionid = "SESS"
    timestamp = 0.1
    applicationid = "APP1"
    contextid = "CONT"
    log_level = DltLogLevel.INFO
    verbose = False

    payload_type = DltPayloadType.STRING
    record_type, msb_first, payload = generate_payload(payload_type=payload_type,
                                                       payload_text=payload_text,
                                                       endianess=endianess)
    payload = bytearray(payload)
    payload[:4] = (0xFF, 0xFF, 0xFF, 0xFF)
    flags = DltHeaderBit(0)
    if msb_first:
        flags |= DltHeaderBit.MSB_FIRST

    number_of_arguments = 1
    extended_header = generate_extended_header(record_type=record_type,
                                               applicationid=applicationid,
                                               contextid=contextid,
                                               log_level=log_level,
                                               verbose=verbose,
                                               number_of_arguments=number_of_arguments,
                                               )
    flags |= DltHeaderBit.USE_EXTENDED_HEADER

    extra_flags, standard_header_extra = generate_standard_header_extra(ecuid=ecuid,
                                                                        sessionid=sessionid,
                                                                        timestamp=timestamp)

    flags |= extra_flags
    standard_header = generate_standard_header(flags=flags,
                                               record_count=1,
                                               record_length=len(payload) + len(extended_header) + len(
                                                   standard_header_extra) + 4
                                               )
    data.extend(standard_header)
    data.extend(standard_header_extra)
    data.extend(extended_header)
    data.extend(payload)
    return data

def gen_incomplete_dlt_msg() -> bytearray:
    data = bytearray()
    data.extend(DLT_MAGIC_SERIAL)
    payload_text = "some text in invalid frame"
    endianess = "little"
    ecuid = "ECU1"
    sessionid = "SESS"
    timestamp = 0.1
    applicationid = "APP1"
    contextid = "CONT"
    log_level = DltLogLevel.INFO
    verbose = False

    payload_type = DltPayloadType.STRING
    record_type, msb_first, payload = generate_payload(payload_type=payload_type,
                                                       payload_text=payload_text,
                                                       endianess=endianess)
    flags = DltHeaderBit(0)
    if msb_first:
        flags |= DltHeaderBit.MSB_FIRST

    number_of_arguments = 1
    extended_header = generate_extended_header(record_type=record_type,
                                               applicationid=applicationid,
                                               contextid=contextid,
                                               log_level=log_level,
                                               verbose=verbose,
                                               number_of_arguments=number_of_arguments,
                                               )
    flags |= DltHeaderBit.USE_EXTENDED_HEADER

    extra_flags, standard_header_extra = generate_standard_header_extra(ecuid=ecuid,
                                                                        sessionid=sessionid,
                                                                        timestamp=timestamp)

    flags |= extra_flags
    standard_header = generate_standard_header(flags=flags,
                                               record_count=1,
                                               record_length=len(payload) + len(extended_header) + len(
                                                   standard_header_extra) + 4 + 100  # additional length which is expected
                                               )
    data.extend(standard_header)
    data.extend(standard_header_extra)
    data.extend(extended_header)
    data.extend(payload)
    return data

class SerialDltMock(Serial):
    """
    A mock object that is basically a serial port
    """

    def __init__(self,
                 file: Optional[Path] = None,
                 *args,
                 **kwargs):
        self.buffer = bytearray()
        self.file = file
        if self.file is not None:
            with self.file.open("rb") as fp:
                self.buffer = fp.read()
        self.cause_resync = False
        self.cause_incomplete = False
        self.cause_invalid = False
        self.cause_invalid_payload = False
        self.silent = False

    def read(self, size=1):
        """
        A self filling buffer that will return the number of bytes to be read.
        :param size: The size to be read.
        :return: The bytes.
        """
        if len(self.buffer) == 0 \
                and not self.silent \
                and self.file is None:
            time.sleep(0.1)
            data = gen_dlt_msg()
            if self.cause_resync:
                # strip the Marker and cause resync
                data = data[len(DLT_MAGIC_SERIAL):]
                self.cause_resync = False
            elif self.cause_invalid:
                # set all bits in Record Type and cause ValueError from Enum
                data = gen_invalid_dlt_msg()
                self.cause_invalid = False
            elif self.cause_invalid_payload:
                data = gen_invalid_payload_dlt_msg()
                self.cause_invalid_payload = False
            elif self.cause_incomplete:
                data = gen_incomplete_dlt_msg()
                self.cause_incomplete = False

            self.buffer.extend(data)
        buffer = bytes(self.buffer[:size])
        self.buffer = bytearray(self.buffer[size:])
        return buffer

    def flush(self):
        return

    def inWaiting(self):
        if self.file is not None:
            return min(randint(100, 200), len(self.buffer))
        else:
            return len(self.buffer)

    def open(self):
        return None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def cause_resync_to_dlt_magic(self):
        self.cause_resync = True

    def cause_incomplete_record_read(self):
        self.cause_incomplete = True

    def cause_silence(self):
        self.silent = False

    def cause_invalid_record_read(self):
        self.cause_invalid = True

    def cause_invalid_payload_read(self):
        self.cause_invalid_payload = True


class SocketDltMock:

    def __init__(self, port=3490, host="localhost"):
        self.buffer = bytearray()
        self.cause_resync = False
        self.cause_incomplete = False
        self.silent = False

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((host, port))
        self.sock.listen(2)
        self.txqueue = Queue()
        self.listeners = []
        self.connhandler = threading.Thread(target=self.handle_connections)
        self.connhandler.daemon = True
        self.connhandler.start()
        self.pushhandler = threading.Thread(target=self.pushoutmessage)
        self.pushhandler.daemon = True
        self.pushhandler.start()

    def handle_connections(self):
        while True:
            clientsocket, addr = self.sock.accept()
            self.listeners.append((clientsocket, addr))

    def pushoutmessage(self):
        while True:
            size = len(self.buffer)
            if len(self.buffer) == 0 and not self.silent:
                time.sleep(0.1)
                data = gen_dlt_msg()
                if self.cause_resync:
                    self.buffer.extend(data[len(DLT_MAGIC_SERIAL):])
                    self.cause_resync = False
                self.buffer.extend(data)
                size = len(self.buffer)
            buffer = bytes(self.buffer[:size])
            self.buffer = bytearray(self.buffer[size:])
            for clientsocket, addr in self.listeners:
                clientsocket.sendall(buffer)

    def __del__(self):
        self.sock.close()

    def cause_resync_to_dlt_magic(self):
        self.cause_resync = True

    def cause_incomplete_record_read(self):
        self.cause_incomplete = True

    def cause_silence(self):
        self.silent = False
